package ru.tsc.kitaev.tm.bootstrap;

import lombok.SneakyThrows;
import org.apache.activemq.ActiveMQConnection;
import org.apache.activemq.ActiveMQConnectionFactory;
import org.apache.log4j.BasicConfigurator;
import org.jetbrains.annotations.NotNull;
import ru.tsc.kitaev.tm.listener.EntityListener;
import ru.tsc.kitaev.tm.service.LoggerService;

import javax.jms.*;

public final class Bootstrap {

    @NotNull
    private static final String URL = ActiveMQConnection.DEFAULT_BROKER_URL;

    @NotNull
    private static final String QUEUE = "LOGGER";

    @SneakyThrows
    public void start() {
        BasicConfigurator.configure();
        @NotNull final LoggerService loggerService = new LoggerService();
        @NotNull final EntityListener entityListener = new EntityListener(loggerService);
        @NotNull final ConnectionFactory connectionFactory = new ActiveMQConnectionFactory(URL);
        @NotNull final Connection connection = connectionFactory.createConnection();
        connection.start();
        @NotNull final Session session = connection.createSession(false, Session.AUTO_ACKNOWLEDGE);
        @NotNull final Queue destination = session.createQueue(QUEUE);
        @NotNull final MessageConsumer messageConsumer = session.createConsumer(destination);
        messageConsumer.setMessageListener(entityListener);
    }

}
