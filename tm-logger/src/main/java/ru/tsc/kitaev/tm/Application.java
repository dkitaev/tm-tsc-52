package ru.tsc.kitaev.tm;

import org.jetbrains.annotations.NotNull;
import ru.tsc.kitaev.tm.bootstrap.Bootstrap;

public final class Application {

    public static void main(String[] args) {
        @NotNull final Bootstrap bootstrap = new Bootstrap();
        bootstrap.start();
    }

}
