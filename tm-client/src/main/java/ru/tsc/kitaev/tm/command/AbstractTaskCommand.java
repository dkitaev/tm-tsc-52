package ru.tsc.kitaev.tm.command;

import org.jetbrains.annotations.Nullable;
import ru.tsc.kitaev.tm.endpoint.TaskDTO;
import ru.tsc.kitaev.tm.exception.entity.TaskNotFoundException;

public abstract class AbstractTaskCommand extends AbstractCommand {

    protected void showTask(@Nullable TaskDTO task) {
        if (task == null) throw new TaskNotFoundException();
        System.out.println("Id: " + task.getId());
        System.out.println("Name: " + task.getName());
        System.out.println("Description: " + task.getDescription());
        System.out.println("Status: " + task.getStatus());
    }

}
