package ru.tsc.kitaev.tm.api.repository.model;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.tsc.kitaev.tm.model.Session;

public interface ISessionRepository {

    void add(@NotNull final Session session);

    void clear();

    @Nullable
    Session findById(@NotNull final String id);

    void removeById(@NotNull final String id);

}
