package ru.tsc.kitaev.tm.api.service;

import lombok.SneakyThrows;
import org.jetbrains.annotations.NotNull;

import javax.persistence.EntityManager;
import javax.persistence.EntityManagerFactory;

public interface IConnectionService {

    @NotNull
    EntityManager getEntityManager();

    @NotNull
    EntityManagerFactory factory();

    @SneakyThrows
    void close();

}
