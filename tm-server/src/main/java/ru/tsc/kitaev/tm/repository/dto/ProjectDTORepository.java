package ru.tsc.kitaev.tm.repository.dto;

import org.hibernate.jpa.QueryHints;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.tsc.kitaev.tm.api.repository.dto.IProjectDTORepository;
import ru.tsc.kitaev.tm.dto.ProjectDTO;
import ru.tsc.kitaev.tm.exception.entity.ProjectNotFoundException;

import javax.persistence.EntityManager;
import java.util.List;

public class ProjectDTORepository implements IProjectDTORepository {

    protected final EntityManager entityManager;

    public ProjectDTORepository(@NotNull final EntityManager entityManager) {
        this.entityManager = entityManager;
    }

    @Override
    public void add(@NotNull final ProjectDTO project) {
        entityManager.persist(project);
    }

    @Override
    public void update(@NotNull final ProjectDTO project) {
        entityManager.merge(project);
    }

    @Override
    public void clear() {
        entityManager.createQuery("DELETE FROM ProjectDTO").executeUpdate();
    }

    @Override
    public void clearByUserId(@NotNull final String userId) {
        entityManager
                .createQuery("DELETE FROM ProjectDTO p WHERE p.userId = :userId")
                .setParameter("userId", userId)
                .executeUpdate();
    }

    @Override
    @NotNull
    public List<ProjectDTO> findAll() {
        return entityManager
                .createQuery("FROM ProjectDTO", ProjectDTO.class)
                .setHint(QueryHints.HINT_CACHEABLE, true)
                .getResultList();
    }

    @Override
    @NotNull
    public List<ProjectDTO> findAllByUserId(@NotNull final String userId) {
        return entityManager
                .createQuery("FROM ProjectDTO p WHERE p.userId = :userId", ProjectDTO.class)
                .setHint(QueryHints.HINT_CACHEABLE, true)
                .setParameter("userId", userId)
                .getResultList();
    }

    @Override
    @Nullable
    public ProjectDTO findById(@NotNull final String userId, @NotNull final String id) {
        return entityManager
                .createQuery("FROM ProjectDTO p WHERE p.userId = :userId AND p.id = :id", ProjectDTO.class)
                .setHint(QueryHints.HINT_CACHEABLE, true)
                .setParameter("userId", userId)
                .setParameter("id", id)
                .getResultStream()
                .findFirst()
                .orElseThrow(ProjectNotFoundException::new);
    }

    @Override
    @NotNull
    public ProjectDTO findByIndex(@NotNull final String userId, @NotNull final Integer index) {
        return entityManager
                .createQuery("FROM ProjectDTO p WHERE p.userId = :userId", ProjectDTO.class)
                .setParameter("userId", userId)
                .setFirstResult(index)
                .setMaxResults(1)
                .getResultStream()
                .findFirst()
                .orElseThrow(ProjectNotFoundException::new);
    }

    @Override
    public void removeById(@NotNull final String userId, @NotNull final String id) {
        entityManager.remove(findById(userId, id));
    }

    @Override
    public void removeByIndex(@NotNull final String userId, @NotNull final Integer index) {
        entityManager.remove(findByIndex(userId, index));
    }

    @Override
    @NotNull
    public Integer getSize(@NotNull final String userId) {
        return entityManager
                .createQuery("SELECT COUNT(p) FROM ProjectDTO p WHERE p.userId = :userId", Long.class)
                .setParameter("userId", userId)
                .getSingleResult()
                .intValue();
    }

    @Override
    @NotNull
    public ProjectDTO findByName(@NotNull final String userId, @NotNull final String name) {
        return entityManager
                .createQuery("FROM ProjectDTO p WHERE p.userId = :userId AND p.name = :name", ProjectDTO.class)
                .setParameter("userId", userId)
                .setParameter("name", name)
                .getResultStream()
                .findFirst()
                .orElseThrow(ProjectNotFoundException::new);
    }

    @Override
    public void removeByName(@NotNull final String userId, @NotNull final String name) {
        entityManager.remove(findByName(userId, name));
    }

}
