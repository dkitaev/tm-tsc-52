package ru.tsc.kitaev.tm.enumerated;

import org.jetbrains.annotations.NotNull;

public enum Status {

    NOT_STARTED("Not started"),
    IN_PROGRESS("In Progress"),
    COMPLETED("Complete");

    @NotNull
    private final String displayName;

    Status(@NotNull String displayName) {
        this.displayName = displayName;
    }

    @NotNull
    public String getDisplayName() {
        return displayName;
    }

}
