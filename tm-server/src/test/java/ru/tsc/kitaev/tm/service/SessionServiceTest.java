package ru.tsc.kitaev.tm.service;

import org.jetbrains.annotations.NotNull;
import org.junit.*;
import org.junit.experimental.categories.Category;
import ru.tsc.kitaev.tm.api.service.dto.ISessionDTOService;
import ru.tsc.kitaev.tm.api.service.dto.IUserDTOService;
import ru.tsc.kitaev.tm.component.Bootstrap;
import ru.tsc.kitaev.tm.enumerated.Role;
import ru.tsc.kitaev.tm.dto.SessionDTO;
import ru.tsc.kitaev.tm.marker.UnitCategory;
import ru.tsc.kitaev.tm.service.dto.SessionDTOService;
import ru.tsc.kitaev.tm.service.dto.UserDTOService;

public class SessionServiceTest {

    @NotNull
    private final ISessionDTOService sessionService;

    @NotNull
    private SessionDTO session;

    @NotNull
    private final IUserDTOService userService;

    @NotNull
    private final String userLogin = "test";

    @NotNull
    private final String userPassword = "test";

    @NotNull
    static final PropertyService PROPERTY_SERVICE = new PropertyService();

    @NotNull
    static final ConnectionService CONNECTION_SERVICE = new ConnectionService(PROPERTY_SERVICE);

    public SessionServiceTest() {
        @NotNull final LoggerService loggerService = new LoggerService(CONNECTION_SERVICE);
        sessionService = new SessionDTOService(CONNECTION_SERVICE, loggerService, new Bootstrap());
        userService = new UserDTOService(CONNECTION_SERVICE, loggerService, PROPERTY_SERVICE);
        userService.create(userLogin, userPassword);
    }

//    @Before
//    public void before() {
//        session = sessionService.open(userLogin, userPassword);
//    }

    @Test
    @Category(UnitCategory.class)
    public void openTest() {
        final int initialSize = sessionService.getSize();
        @NotNull final SessionDTO session = sessionService.open("test", "test");
        Assert.assertEquals(initialSize + 1, sessionService.getSize());
        Assert.assertNotNull(session.getSignature());
    }

    @Test
    @Category(UnitCategory.class)
    public void closeTest() {
        session = sessionService.open(userLogin, userPassword);
        final int initialSize = sessionService.getSize();
        @NotNull final SessionDTO session = sessionService.findAll().get(0);
        sessionService.close(session);
        Assert.assertEquals(initialSize - 1, sessionService.getSize());
    }

    @Test
    @Category(UnitCategory.class)
    public void validateTest() {
        @NotNull final SessionDTO session = sessionService.open("admin", "admin");
        sessionService.validate(session);
    }

    @Test
    @Category(UnitCategory.class)
    public void validateRoleTest() {
        @NotNull final SessionDTO session = sessionService.open("admin", "admin");
        sessionService.validate(session, Role.ADMIN);
    }

    @After
    public void after() {
        sessionService.close(session);
    }

    @AfterClass
    public static void afterClass() {
        CONNECTION_SERVICE.close();
    }

}
