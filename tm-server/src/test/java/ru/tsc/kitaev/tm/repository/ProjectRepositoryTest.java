package ru.tsc.kitaev.tm.repository;

import org.jetbrains.annotations.NotNull;
import org.junit.After;
import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;
import org.junit.experimental.categories.Category;
import ru.tsc.kitaev.tm.api.repository.dto.IProjectDTORepository;
import ru.tsc.kitaev.tm.api.repository.dto.IUserDTORepository;
import ru.tsc.kitaev.tm.api.service.IConnectionService;
import ru.tsc.kitaev.tm.api.service.IPropertyService;
import ru.tsc.kitaev.tm.dto.ProjectDTO;
import ru.tsc.kitaev.tm.dto.UserDTO;
import ru.tsc.kitaev.tm.marker.UnitCategory;
import ru.tsc.kitaev.tm.repository.dto.ProjectDTORepository;
import ru.tsc.kitaev.tm.repository.dto.UserDTORepository;
import ru.tsc.kitaev.tm.service.ConnectionService;
import ru.tsc.kitaev.tm.service.PropertyService;
import ru.tsc.kitaev.tm.util.HashUtil;

import javax.persistence.EntityManager;

public class ProjectRepositoryTest {

    @NotNull
    private final EntityManager entityManager;

    @NotNull
    private final IProjectDTORepository projectRepository;

    @NotNull
    private final IUserDTORepository userRepository;

    @NotNull
    private final ProjectDTO project;

    @NotNull
    private final String projectId;

    @NotNull
    private final String projectName = "testProject";

    @NotNull
    private final String projectDescription = "testProjectDescription";

    @NotNull
    private final UserDTO user;

    @NotNull
    private final String userId;

    public ProjectRepositoryTest() {
        @NotNull final IPropertyService propertyService = new PropertyService();
        @NotNull final IConnectionService connectionService = new ConnectionService(propertyService);
        entityManager = connectionService.getEntityManager();
        projectRepository = new ProjectDTORepository(entityManager);
        userRepository = new UserDTORepository(entityManager);
        user = new UserDTO();
        userId = user.getId();
        user.setLogin("test");
        user.setPasswordHash(HashUtil.salt("test", 5, "test"));
        project = new ProjectDTO();
        projectId = project.getId();
        project.setUserId(userId);
        project.setName(projectName);
        project.setDescription(projectDescription);
        entityManager.getTransaction().begin();
        userRepository.add(user);
        entityManager.getTransaction().commit();
    }

    @Before
    public void before() {
        entityManager.getTransaction().begin();
        projectRepository.add(project);
        entityManager.getTransaction().commit();
    }

    @Test
    @Category(UnitCategory.class)
    public void findProjectTest() {
        Assert.assertNotNull(project);
        Assert.assertNotNull(userId);
        Assert.assertNotNull(projectId);
        Assert.assertNotNull(projectName);
        Assert.assertEquals(project, projectRepository.findById(userId, projectId));
        Assert.assertEquals(project, projectRepository.findByIndex(userId, 0));
        Assert.assertEquals(project, projectRepository.findByName(userId, projectName));
    }

    @Test
    @Category(UnitCategory.class)
    public void removeProjectByIdTest() {
        Assert.assertNotNull(project);
        Assert.assertNotNull(userId);
        Assert.assertNotNull(projectId);
        entityManager.getTransaction().begin();
        projectRepository.removeById(userId, projectId);
        entityManager.getTransaction().commit();
        Assert.assertTrue(projectRepository.findAllByUserId(userId).isEmpty());
    }

    @Test
    @Category(UnitCategory.class)
    public void removeProjectByIndexTest() {
        Assert.assertNotNull(project);
        Assert.assertNotNull(userId);
        entityManager.getTransaction().begin();
        projectRepository.removeByIndex(userId, 0);
        entityManager.getTransaction().commit();
        Assert.assertTrue(projectRepository.findAllByUserId(userId).isEmpty());
    }

    @Test
    @Category(UnitCategory.class)
    public void removeProjectByNameTest() {
        Assert.assertNotNull(project);
        Assert.assertNotNull(userId);
        Assert.assertNotNull(projectName);
        entityManager.getTransaction().begin();
        projectRepository.removeByName(userId, projectName);
        entityManager.getTransaction().commit();
        Assert.assertTrue(projectRepository.findAllByUserId(userId).isEmpty());
    }

    @After
    public void after() {
        entityManager.getTransaction().begin();
        projectRepository.clearByUserId(userId);
        userRepository.remove(user);
        entityManager.getTransaction().commit();
        entityManager.close();
    }

}
